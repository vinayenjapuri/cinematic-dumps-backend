import express from "express";
import bodyParser from "body-parser";
import routes from './src/routes/cinematicDumpRoutes';

const app = express();
const PORT = process.env.PORT || 4000;
const cors = require('cors');

export var cinematicDumpsAggregate = {}

export async function connectionDataBase() {
  const MongoClient = require('mongodb').MongoClient;
  const uri = "mongodb+srv://vinayenjapuri:Omika%401309@easygo.bq3hogt.mongodb.net/?retryWrites=true&w=majority";
  const client = new MongoClient(uri, { useNewUrlParser: true });
  const connection = await client.connect();
  const storeCollectionData = async (collection) => {
    try {
      const session = connection.startSession();
      const coll = connection.db('moviestore').collection(collection);
      const documents = await coll.find({}).toArray();
      session.endSession();
      return documents;
    } catch (err) {
      return [];
    }
  }
  ['movies', 'animes', 'mangas', 'series'].forEach(async (collection) => {
    cinematicDumpsAggregate[collection] = await storeCollectionData(collection);
  });
}

connectionDataBase();

// bodyParser setup
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());


//Ex: http://localhost:4000/images/paid-service-logos/netflix.png

routes(app);

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`);
});



