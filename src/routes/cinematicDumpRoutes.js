import {addNewMovies, getAllMovies, addNewAnimes, addNewMangas, addNewSeries, getAllAnimes, getAllSeries, getAllMangas} from '../controllers/cinematicDumpControllers';

const multer = require('multer');
const routes = (app) => {
    app.route('/movies-aggregate')
    .get(getAllMovies)
    .post(addNewMovies)

    app.route('/animes-aggregate')
    .get(getAllAnimes)
    .post(addNewAnimes)

    app.route('/mangas-aggregate')
    .get(getAllMangas)
    .post(addNewMangas)

    app.route('/series-aggregate')
    .get(getAllSeries)
    .post(addNewSeries)
}

export default routes;
