import {cinematicDumpsAggregate} from '../../server';

//add a new document
export async function addDocumentInCollection(body, res, collection) {
    cinematicDumpsAggregate[collection]?.push(body);
    res.send(cinematicDumpsAggregate[collection]);
    const connection = await databaseConnection();
   
    try {
        const session = connection.startSession();
        const coll = connection.db('moviestore').collection(collection);
        const success = coll.insertMany([body]);
        success.catch(err => {
            cinematicDumpsAggregate[collection].pop();
        });
        session.endSession();
      } catch (err) {
        res.send([err, "Error"]);
      }
}


//get and show all documents
export async function showDocumentsOfCollection(res, collection) {
    if (cinematicDumpsAggregate[collection]?.length > 0) {
        res.send(cinematicDumpsAggregate[collection]);
    } else {
        const connection = await databaseConnection();
    try {
        const session = connection.startSession();
        const coll = connection.db('moviestore').collection(collection);
        const documents = await coll.find({}, {_id: 0}).toArray();
        res.json(documents);
        session.endSession();
    } catch (err) {
        res.send([err, "Error"]);
    }
  }
}

async function databaseConnection() {
    const MongoClient = require('mongodb').MongoClient;
    const uri = "mongodb+srv://vinayenjapuri:Omika%401309@easygo.bq3hogt.mongodb.net/?retryWrites=true&w=majority";
    const client = new MongoClient(uri, { useNewUrlParser: true });
    const connection = await client.connect();
    return connection;
  }
