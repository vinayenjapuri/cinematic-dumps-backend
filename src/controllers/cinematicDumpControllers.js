import multer from 'multer';
import {addDocumentInCollection, showDocumentsOfCollection} from  '../dbConfiguration/db';

// POST call
// add a new movie document
export const addNewMovies =  async (req, res) => {
    await addDocumentInCollection(req.body, res, 'movies');
}

// POST call
// add a new series document
export const addNewSeries =  async (req, res) => {
    await addDocumentInCollection(req.body, res, 'series');
}

// POST call
// add a new anime document
export const addNewAnimes =  async (req, res) => {
    await addDocumentInCollection(req.body, res, 'animes');
}

// POST call
// add a new manga document
export const addNewMangas =  async (req, res) => {
    await addDocumentInCollection(req.body, res, 'mangas');
}

// POST call
// add a new review document
export const addNewReviews =  async (req, res) => {
    await addDocumentInCollection(req.body, res, 'reviews');
}

// GET call
// show all documents in movies collection
export const getAllMovies = async (req, res) => {
    await showDocumentsOfCollection(res, 'movies');
}

// GET call
// show all documents in animes collection
export const getAllAnimes = async (req, res) => {
    await showDocumentsOfCollection(res, 'animes');
}

// GET call
// show all documents in mangas collection
export const getAllMangas = async (req, res) => {
    await showDocumentsOfCollection(res, 'mangas');
}

// GET call
// show all documents in series collection
export const getAllSeries = async (req, res) => {
    await showDocumentsOfCollection(res, 'series');
}
